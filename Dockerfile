#Specify Base Image
From node:alpine

WORKDIR /kuliah/TelkomDBT/tugas2project
COPY ./package.json ./

# Install Dependencies
RUN npm install 

# Copy Folder and File Source Code

COPY ./ ./

#Default command
CMD ["npm","start"]